import os
import argparse
from PIL import Image


def crop_and_create_favicon(image_path, output_dir):
    """
    Crop an image to a square by keeping the horizontal center and reducing only the pixels below,
    then create a favicon of 64x64 pixels. Save both images in the specified output directory.

    Args:
        image_path (str): Path to the input image.
        output_dir (str): Directory to save the cropped image and favicon.

    Returns:
        None
    """
    # Ensure output directory exists
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    with Image.open(image_path) as img:
        width, height = img.size
        new_dimension = min(width, height)

        # Calculate the coordinates for cropping
        left = (width - new_dimension) // 2
        upper = 0
        right = left + new_dimension
        lower = new_dimension

        # Perform the crop
        cropped_img = img.crop((left, upper, right, lower))

        # Save the cropped image as "photo.jpg"
        photo_path = os.path.join(output_dir, "photo.jpg")
        cropped_img.save(photo_path, format="JPEG")

        # Resize the cropped image to 64x64 for the favicon
        favicon = cropped_img.resize((64, 64), Image.Resampling.BILINEAR)

        # Save the favicon as "favicon.ico"
        favicon_path = os.path.join(output_dir, "favicon.ico")
        favicon.save(favicon_path, format="ICO")


def main():
    parser = argparse.ArgumentParser(
        description="Crop an image to a square and create a 64x64 favicon."
    )
    parser.add_argument("image_path", type=str, help="Path to the input image")

    args = parser.parse_args()

    crop_and_create_favicon(args.image_path, "web/images")


if __name__ == "__main__":
    main()
