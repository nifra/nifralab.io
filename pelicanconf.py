#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = "Nicolas Maignan"
SITENAME = "Nicolas Maignan"
SITEURL = ""
SITETITLE = "Nicolas Maignan"
SITESUBTITLE = "PhD Student"
SITELOGO = SITEURL + "/images/photo.jpg"
FAVICON = SITEURL + "/images/favicon.ico"

THEME = "/tmp/Flex"
PATH = "web"

TIMEZONE = "Europe/Paris"

DEFAULT_LANG = "en"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (("My Univ GitLab", "https://gitlab.univ-lorraine.fr/maignan2"),)

# Social widget
SOCIAL = (
    ("envelope", "mailto:nicolas.maignan@univ-lorraine.fr"),
    ("orcid", "https://orcid.org/0000-0003-2660-7110"),
    ("gitlab", "https://gitlab.com/nifra"),
    ("discord", "https://discord.com/users/163192855802675200"),
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True
