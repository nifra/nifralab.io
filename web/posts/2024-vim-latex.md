Title: Writing LaTeX with Vim
Date: 2024-07-09
Slug: vim-latex
Lang: en

<!-- markdownlint-disable MD013 -->

Over the last few months, I've changed IDEs, switching from VSCode to NeoVim, because this editor has always had my affection for its lightness and comfort in the terminal. So I went from an editor that I'd configured to write mainly Python to a multi-language editor, powerful because of the keyboard control and the multiple, easily configurable plugins. However, I always wrote my LaTeX files on Overleaf, even at the draft stage, when I wasn't yet sharing them with my supervisors. That's why I wanted to move to a local editor that I'm familiar with.

That's where the problems started. Firstly, LaTeX is ... a pain to install. Being a long-established language, created by [Leslie Lamport in the early 80s](https://lamport.azurewebsites.net/pubs/pubs.html?from=https://research.microsoft.com/users/lamport/pubs/pubs.html&type=path#latex), there are numerous compilers more or less recent and more or less documented. For example, I found [tectonic](https://tectonic-typesetting.github.io/en-US/) very interesting because with a simple compile call `tectonic my_file.tex`, all the additional libraries are downloaded automatically, unlike the canonical [texlive](https://www.tug.org/texlive/) where the multiple plugins have to be installed a priori (and therefore a lengthy installation) and which suffers from a fairly diverse package per distribution.
What's more, LaTeX support for Vim also varies between full VimScript plugins and LSPs.

Finally, the most compatible system I've found between compiling and editing is simply to use `latexmk`, which can be directly installed as a package under Debian/Ubuntu (even if you also need to install `texlive-full` to make sure you have all the right plugins) and for NeoVim, the [vimtex](https://github.com/lervag/vimtex) plugin which integrates very well with NeoVim plugins, for automatic completion for example, or compiling on save. This allows me to edit in Vim with completion, colours and vim motion while benefiting from automatic compilation and preview with my favourite document reader, [zathura](https://pwmt.org/projects/zathura/).
