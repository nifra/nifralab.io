Title: Hello World!
Date: 2024-03-14
Slug: hello-world
Lang: en

The first post, but more importantly, the launch of my website. So you'll always know where to find my news!

For the moment the site is hosted on GitLab, but that could change in the future.
