Title: Contact

You can contact me at the following email addresses:

| usage      | email                              | aliases                    |
| :--------- | :--------------------------------- | :------------------------- |
| personal   | <nicolas.maignan@proton.me>        |                            |
| university | <nicolas.maignan@univ-lorraine.fr> |                            |
| laboratory | <nicolas.maignan@inria.fr>         | <nicolas.maignan@loria.fr> |
